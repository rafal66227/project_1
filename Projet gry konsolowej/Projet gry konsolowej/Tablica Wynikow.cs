﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace THE_GAME
{

    public class tablica_wynikow
    {

        public static int odczytaj_wartosc(int minigra, int level)
        {
            int x = 0;


            switch (minigra)
            {
                case 1:
                    {

                        if (level == 1)
                        {

                            string nazwaPliku = @"save\heist-easy.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 2)
                        {

                            string nazwaPliku = @"save\heist-normal.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 3)
                        {

                            string nazwaPliku = @"save\heist-hardcore.txt";


                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                    }
                    break;

                case 2:
                    {

                        if (level == 1)
                        {

                            string nazwaPliku = @"save\running-easy.txt";

                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 2)
                        {

                            string nazwaPliku = @"save\running-normal.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 3)
                        {

                            string nazwaPliku = @"save\running-hardcore.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                    }
                    break;

                case 3:
                    {

                        if (level == 1)
                        {

                            string nazwaPliku = @"save\maze-easy.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 2)
                        {

                            string nazwaPliku = @"save\maze-normal.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 3)
                        {

                            string nazwaPliku = @"save\maze-hardcore.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            x = Convert.ToInt32(odczyt.ReadLine());

                            odczyt.Close();
                            plik.Close();

                        }

                    }
                    break;

            }

            return x;
        }

        public static void wpisz_wynik(int minigra, int level, string player, int wynik)
        {

            switch (minigra)

            {

                case 1:

                    {
                        if (level == 1)

                        {
                            string nazwaPliku = @"save\heist-easy.txt";
                            string nazwaPliku2 = @"save\heist-easy-name.txt";

                            int temp = odczytaj_wartosc(1, 1);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }

                        if (level == 2)

                        {
                            string nazwaPliku = @"save\heist-normal.txt";
                            string nazwaPliku2 = @"save\heist-normal-name.txt";

                            int temp = odczytaj_wartosc(1, 2);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }

                        if (level == 3)

                        {
                            string nazwaPliku = @"save\heist-hardcore.txt";
                            string nazwaPliku2 = @"save\heist-hardcore-name.txt";

                            int temp = odczytaj_wartosc(1, 3);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }
                    }
                    break;


                case 2:

                    {
                        if (level == 1)

                        {
                            string nazwaPliku = @"save\running-easy.txt";
                            string nazwaPliku2 = @"save\running-easy-name.txt";

                            int temp = odczytaj_wartosc(2, 1);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }

                        if (level == 2)

                        {
                            string nazwaPliku = @"save\running-normal.txt";
                            string nazwaPliku2 = @"save\running-normal-name.txt";

                            int temp = odczytaj_wartosc(2, 2);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }

                        if (level == 3)

                        {
                            string nazwaPliku = @"save\running-hardcore.txt";
                            string nazwaPliku2 = @"save\running-hardcore-name.txt";

                            int temp = odczytaj_wartosc(2, 3);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }
                    }
                    break;
                case 3:

                    {
                        if (level == 1)

                        {
                            string nazwaPliku = @"save\maze-easy.txt";
                            string nazwaPliku2 = @"save\maze-easy-name.txt";

                            int temp = odczytaj_wartosc(3, 1);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }

                        if (level == 2)

                        {
                            string nazwaPliku = @"save\maze-normal.txt";
                            string nazwaPliku2 = @"save\maze-normal-name.txt";

                            int temp = odczytaj_wartosc(3, 2);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }

                        if (level == 3)

                        {
                            string nazwaPliku = @"save\maze-hardcore.txt";
                            string nazwaPliku2 = @"save\maze-hardcore-name.txt";

                            int temp = odczytaj_wartosc(3, 3);

                            
                                FileStream plik = new FileStream(nazwaPliku, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis = new StreamWriter(plik);

                                zapis.Write(wynik);
                                zapis.Close();
                                plik.Close();

                                FileStream plik2 = new FileStream(nazwaPliku2, FileMode.Create, FileAccess.Write, FileShare.None);
                                System.IO.StreamWriter zapis2 = new StreamWriter(plik2);

                                zapis2.Write(player);
                                zapis2.Close();
                                plik2.Close();
                            
                        }
                    }
                    break;
            }
        }

        public static string odczytaj_imie(int minigra, int level)
        {
            string player = "";


            switch (minigra)
            {

                case 1:
                    {

                        if (level == 1)
                        {

                            string nazwaPliku = @"save\heist-easy-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 2)
                        {

                            string nazwaPliku = @"save\heist-normal-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 3)
                        {

                            string nazwaPliku = @"save\heist-hardcore-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                    }
                    break;

                case 2:
                    {

                        if (level == 1)
                        {

                            string nazwaPliku = @"save\running-easy-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 2)
                        {

                            string nazwaPliku = @"save\running-normal-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 3)
                        {

                            string nazwaPliku = @"save\running-hardcore-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                    }
                    break;

                case 3:
                    {

                        if (level == 1)
                        {

                            string nazwaPliku = @"save\maze-easy-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 2)
                        {

                            string nazwaPliku = @"save\maze-normal-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                        if (level == 3)
                        {

                            string nazwaPliku = @"save\maze-hardcore-name.txt";
                            FileStream plik = new FileStream(nazwaPliku, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                            System.IO.StreamReader odczyt = new StreamReader(plik);

                            player = odczyt.ReadLine();

                            odczyt.Close();
                            plik.Close();

                        }

                    }
                    break;

            }

            return player;
        }

        public static void wyswietl_rekord(int minigra)
        {
            int temp = 0;
            do
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("\n\n      NAJLEPSZY OSIĄGNIĘTY WYNIK NA POZIOMIE TRUDNOŚCI\n\n");
                Console.ResetColor();
                Console.WriteLine("              WYNIK      GRACZ\n\n");


                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("  ŁATWY:     ");
                Console.WriteLine("    {0}      {1}\n\n", tablica_wynikow.odczytaj_wartosc(minigra, 1), tablica_wynikow.odczytaj_imie(minigra, 1));

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("  NORMALNY:  ");
                Console.WriteLine("    {0}      {1}\n\n", tablica_wynikow.odczytaj_wartosc(minigra, 2), tablica_wynikow.odczytaj_imie(minigra, 2));

                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("  HARDKOROWY:");
                Console.WriteLine("    {0}      {1}\n\n", tablica_wynikow.odczytaj_wartosc(minigra, 3), tablica_wynikow.odczytaj_imie(minigra, 3));

                Console.ResetColor();

                Console.WriteLine("<Wciśnij Delete, aby zresetować wyniki>");
                Console.WriteLine("<Wciśnij Escape, aby wrócić do menu>");
                
                var key = Console.ReadKey(true).Key;
                temp = Convert.ToInt16(key);
                

                if (temp == 46) { for (int i = 1; i <= 3; i++) { wpisz_wynik(minigra, i, "", 0); } }
                
            }

            while (temp != 27);
        }
    }
}