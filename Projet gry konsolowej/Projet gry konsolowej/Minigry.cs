﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace THE_GAME
{
    public class minigame
    {
        public object locker = new object();


        public static int zmien_kod_v1(int x)
        {
            int kod = 0;

            switch (x)
            {
                case 1: kod = 49; break;
                case 2: kod = 50; break;
                case 3: kod = 51; break;
                case 4: kod = 52; break;
                case 5: kod = 53; break;
                case 6: kod = 54; break;
                case 7: kod = 55; break;
                case 8: kod = 56; break;
                case 9: kod = 57; break;
            }
            return kod;
        }

        public static int zmien_kod_v2(int x)
        {
            int kod = 0;

            switch (x)
            {
                case 1: kod = 97; break;
                case 2: kod = 98; break;
                case 3: kod = 99; break;
                case 4: kod = 100; break;
                case 5: kod = 101; break;
                case 6: kod = 102; break;
                case 7: kod = 103; break;
                case 8: kod = 104; break;
                case 9: kod = 105; break;
            }
            return kod;
        }

        public static int losuj_cyfre()
        {
            Random r = new Random();
            int x = r.Next(1, 10);
            return x;

        }
        public static int sprawdz_cyfre(int x, int y)
        {
            if (x == y) return 1;
            else return 0;

        }

        public static void Heist(int poziom)
        {

            Console.WriteLine("Celem gry jest prawidłowe zapamiętanie i wpisanie w odpowiedniej kolejności");
            Console.WriteLine("pojawiających się cyfr. Po wpisaniu każdej liczby naciśnij Enter by wpisać ");
            Console.WriteLine("kolejną. Przy każdym nieprawidłowym podaniu kodu ten generuje się na nowo.\nZgarnij jak największą liczbę łupów i nie daj się złapać.");
            Console.WriteLine("");

            /* for (int i = 0; i < 500; i++)
             {
                 int tempu = 0;
                 var key = Console.ReadKey(true).Key;
                 tempu = Convert.ToInt16(key);
                 Console.WriteLine("{0}", tempu);
             }
             */
            int szansa = 3;
            int licznik = 0;
            int czas = 0;
            double level = (poziom * 2);
            czas = Convert.ToInt16(800 / level);
            do
            {
                int temp = 0;
            int cyfra = 0;
            int[] szyfr = new int[5];
            int[] szyfr_kod_v1 = new int[5];
            int[] szyfr_kod_v2 = new int[5];


            int alarm = 0;
            int zgodnosc = 0;

            
                do
                {
                    Console.Clear();
                    Console.WriteLine("<Wciśnij Backspace, aby poznać szyfr>");
                }
                while (Console.ReadKey(true).Key != ConsoleKey.Backspace);
                Console.Clear();
                System.Threading.Thread.Sleep(1000);

                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.ForegroundColor = ConsoleColor.Yellow;
                
                Console.Clear();

                for (int i = 0; i < 5; i++)
                    
                {
                    cyfra = losuj_cyfre();
                    Console.WriteLine("{0}", cyfra);
                    
                    System.Threading.Thread.Sleep(czas);
                    Console.Clear();
                    szyfr[i] = cyfra;
                    System.Threading.Thread.Sleep(czas);

                }
                  Console.ResetColor();

                for (int i = 0; i < 5; i++)
                {
                    szyfr_kod_v1[i] = zmien_kod_v1(szyfr[i]);
                }
                for (int i = 0; i < 5; i++)
                {
                    szyfr_kod_v2[i] = zmien_kod_v2(szyfr[i]);
                }

                /*Console.WriteLine("Twój kod do wpisania:\n");
                for (int i = 0; i < 5; i++)
                {
                    Console.Write("{0} ", easy_kod[i]);
                }*/

                do
                {
                    Console.Clear();
                    Console.WriteLine("Kliknij Backspace a następnie wpisz na klawiaturze kod do sejfu !");

                }
                while (Console.ReadKey(true).Key != ConsoleKey.Backspace);
                Console.Clear();


                int j = 0;
                do
                {

                    var key = Console.ReadKey(true).Key;
                    temp = Convert.ToInt16(key);


                    if (temp == szyfr_kod_v1[j] || temp == szyfr_kod_v2[j]) { Console.Write("* "); zgodnosc++; }
                    else alarm = 1;
                    j++;
                }
                while (alarm != 1 && zgodnosc < 5);

                if (alarm == 1) { Console.Clear(); Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Włączył się alarm !"); Console.ResetColor(); szansa--; Console.WriteLine("Ilość szans: {0}", szansa); System.Threading.Thread.Sleep(3000); Console.Clear(); }



                else
                {
                    licznik++;
                    System.Threading.Thread.Sleep(400);
                    Console.Clear();
                    Console.WriteLine("Sprawdzanie ");
                    for (int i = 0; i < 5; i++)
                    {
                        Console.Write(". ");
                        System.Threading.Thread.Sleep(900);
                    }


                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("BRAWO! Poprawny kod ! Możesz przejść do kolejnej próby :) ");
                    Console.ResetColor();
                    System.Threading.Thread.Sleep(3000);



                }

            } while (szansa > 0);

            int wynik = licznik * 5;

            Console.WriteLine("Koniec gry! Przegrałeś!");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("\nTwój wynik to: {0}\n", wynik);
            System.Threading.Thread.Sleep(2000);
            

            if (wynik > tablica_wynikow.odczytaj_wartosc(1, poziom))

            {
                Console.WriteLine("Pobiłeś rekord! Twoje osiągnięcie {0} pkt na poziomie trudności: {1},\nzostanie zapisane na Tablicy Rekordów!", wynik, menu.poziom(poziom));
                tablica_wynikow.wpisz_wynik(1, poziom, Program.player, wynik);
            }
            else Console.WriteLine("Nie udało Ci się pobić rekordu. Obecny rekord możesz sprawdzić w menu.\n");
            System.Threading.Thread.Sleep(6000);
            do
            {
                Console.Clear();
                Console.WriteLine("<Kliknij Escape aby wrócić do menu>");

            }
            while (Console.ReadKey(true).Key != ConsoleKey.Escape);
            
            menu.menu_glowne();


        }


        public static void Running(double Potrzebne_Wcisniecia, int Czas)
        {
            Potrzebne_Wcisniecia *= Gods.mnoznik;
            Console.Clear();
            Console.WriteLine("PRZYGOTUJ SIĘ...");
            Console.WriteLine("Naciskaj dwa wskazane klawisze najszybciej jak potrafisz.");
            Console.WriteLine("\n<Wciśnij Escape, jeśli jesteś gotowy>");
            while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("SZYBCIEJ!");


            var Key1 = ConsoleKey.UpArrow;
            var Key2 = ConsoleKey.DownArrow;
            var Key3 = ConsoleKey.LeftArrow;
            var Key4 = ConsoleKey.RightArrow;

            int Liczba_Wcisniec = 0;
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Random r = new Random();
            int kierunek = r.Next(1, 3);

            if (kierunek == 1)
            {
                Console.WriteLine("GÓRA||DÓŁ");
                Console.ResetColor();
            }
            if (kierunek == 2)
            {
                Console.WriteLine("LEWO||PRAWO");
                Console.ResetColor();
            }

            while (sw.ElapsedMilliseconds < Czas)
            {
                switch (kierunek)
                {
                    case 1:
                        {
                            Console.Write("GÓRA");
                            if (Key1 == Console.ReadKey(true).Key)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write(" DOBRZE! ");
                                Console.ResetColor();
                                Liczba_Wcisniec++;
                                Console.WriteLine("DÓŁ");
                                if (Key2 == Console.ReadKey(true).Key)
                                {
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.Write(" DOBRZE! ");
                                    Console.ResetColor();
                                    Liczba_Wcisniec++;
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.Write(" ŹLE! ");
                                    Console.ResetColor();
                                    Liczba_Wcisniec--;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write(" ŹLE! ");
                                Console.ResetColor();
                                Liczba_Wcisniec--;
                            }
                            break;
                        }
                    case 2:
                        {
                            Console.Write("LEWO");
                            if (Key3 == Console.ReadKey(true).Key)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write(" DOBRZE! ");
                                Console.ResetColor();
                                Liczba_Wcisniec++;
                                Console.WriteLine("PRAWO");
                                if (Key4 == Console.ReadKey(true).Key)
                                {
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.Write(" DOBRZE! ");
                                    Console.ResetColor();
                                    Liczba_Wcisniec++;
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.Write(" ŹLE! ");
                                    Console.ResetColor();
                                    Liczba_Wcisniec--;
                                }
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write(" ŹLE! ");
                                Console.ResetColor();
                                Liczba_Wcisniec--;
                            }
                            break;
                        }
                }
            }
            sw.Stop();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("STOP\n");
            Console.ResetColor();
            Console.WriteLine("Liczba wciśnięć = {0}", Liczba_Wcisniec);
            Console.WriteLine("Potrzebna liczba wciśnięć = {0}", Potrzebne_Wcisniecia);

            if (Liczba_Wcisniec > tablica_wynikow.odczytaj_wartosc(2, Gods.lvl_trud))

            {
                Console.WriteLine("Pobiłeś rekord! Twoje osiągnięcie {0} pkt na poziomie trudności: {1} ,\nzostanie zapisaane na Tablicy Rekordów!", Liczba_Wcisniec, menu.poziom(Gods.lvl_trud));
                tablica_wynikow.wpisz_wynik(2, Gods.lvl_trud, Program.player, Liczba_Wcisniec);
            }
            else Console.WriteLine("Nie udało Ci się pobić rekordu. Obecny rekord możesz sprawdzić w menu.");
            
            if (Liczba_Wcisniec < Potrzebne_Wcisniecia)
            {
                var Musicx = new System.Media.SoundPlayer();
                Musicx.SoundLocation = @"data/audio/laugh.wav";
                Musicx.Play();
                Console.WriteLine("- PRZEGRAŁEŚ\n\n<Wciśnij Escape, aby opuścić ekran wyników>");
                while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;
                Console.Clear();
                menu.menu_glowne();
            }
            else Console.WriteLine("- WYGRAŁEŚ\n\n<Wciśnij Escape, aby opuścić ekran wyników>");
            while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;
            Console.Clear();
        }

        static int losuj_strzalke()
        {
            Random r = new Random();
            int x = r.Next(1, 5);
            return x;
        }

        static int przechwycony_wynik = 0;

        public static double mnoznik(int level, int type)
        {
            double value = 0;

            switch (type)
            {
                case 1:
                        {
                            if (level == 1) value = 1.2;
                            if (level == 2) value = 1.5;
                            if (level == 3) value = 1.8;
                        }   break;

                case 2:
                        {
                            if (level == 1) value = 0.8;
                            if (level == 2) value = 1;
                            if (level == 3) value = 1.2;
                        }   break;
            }
            return value;
        }

        static int testuj_strzalki()
        {

            Stopwatch zegar = new Stopwatch();

            zegar.Start();

            int test2 = 0, temp1 = 1, temp2 = 1, temp3 = 1, temp4 = 1;

            int i = 0;

            do

            {
                
                int number = losuj_strzalke();

                switch (number)
                {
                    case 1:
                        Console.WriteLine("Kliknij strzałke w górę");
                        
                        var Key2 = Console.ReadKey(true).Key == ConsoleKey.UpArrow;
                        test2 = Convert.ToInt16(Key2);
                        temp1 = test2;

                        break;


                    case 2:
                        Console.WriteLine("Kliknij strzałke w dół");
                        
                        var Key4 = Console.ReadKey(true).Key == ConsoleKey.DownArrow;
                        test2 = Convert.ToInt16(Key4);
                        temp2 = test2;

                        break;


                    case 3:
                        Console.WriteLine("Kliknij strzałke w prawo");
                        
                        var Key6 = Console.ReadKey(true).Key == ConsoleKey.RightArrow;
                        test2 = Convert.ToInt16(Key6);
                        temp3 = test2;

                        break;


                    case 4:
                        Console.WriteLine("Kliknij strzałke w lewo");
                        
                        var Key8 = Console.ReadKey(true).Key == ConsoleKey.LeftArrow;
                        test2 = Convert.ToInt16(Key8);
                        temp4 = test2;

                        break;
                }

                i++;

            } while (temp1 == 1 && temp2 == 1 && temp3 == 1 && temp4 == 1 && zegar.ElapsedMilliseconds <= 30000);

            return i - 1;
        }

        static int start_gry(int level)

        {
            int zwracana = 0;

            

            for (int i = 3; i > 0; i--)
            {
                

                Console.WriteLine();
                Console.ReadKey();
                Console.WriteLine();


                int wynik = testuj_strzalki();
                if (wynik >= 30 * mnoznik(level, 1)) przechwycony_wynik = wynik;

                Console.WriteLine();

                Console.ForegroundColor = ConsoleColor.Yellow;
                
                Console.WriteLine("Twój wynik to: {0}", wynik);
                Console.WriteLine();
                

                if (wynik >= 30 * mnoznik(level, 1)) {  Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine("Udało Ci się przejść ten poziom!"); i = 0; zwracana = 1; }

                else { Console.ForegroundColor = ConsoleColor.Red; Console.WriteLine("Przegrałeś! Aby przejść ten poziom potrzebujesz minimum {0}!", 30 * mnoznik(level, 1)); zwracana = 0; }
                
                Console.WriteLine();

                if (i > 2)
                {
                    Console.WriteLine("Pozostały Ci {0} życia", i - 1);
                    Console.WriteLine();
                    Console.WriteLine("Spóbuj jeszcze raz!");
                }
                if (i == 2)

                {
                    Console.WriteLine("Pozostało Ci {0} życie", i - 1);
                    Console.WriteLine();
                    Console.WriteLine("Spóbuj jeszcze raz!");
                }
            }
           
            return zwracana;
        }

        public static void Maze(int level)
        {
            Console.ForegroundColor = ConsoleColor.Red; 
            Console.WriteLine("Na każdym rozwidleniu dróg musisz kliknąć odpowiednią strzałkę,\njeśli się pomylisz - przegrasz!");
            Console.WriteLine();
            Console.ReadKey();
            Console.WriteLine("Masz 30s na pokonanie labiryntu, a minimalny wynik jaki musisz osiągnąć to: {0}", 30 * mnoznik(level, 1));
            Console.WriteLine();
            Console.ReadKey();
            Console.WriteLine("Masz 3 życia");
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Kliknij klawisz aby zacząć!");


            double kontynuacja = start_gry(level);

            if (kontynuacja != 1)
            {
                Console.ResetColor();
                Console.WriteLine("Żegnaj! :( ");
                if (level > 1) Console.Write("Spróbuj łatwiejszego poziomu! ");
                Console.WriteLine();
                Console.WriteLine("<Wciśnij Escape, aby wrócić do menu>");
                while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;
                Console.Clear();
                Console.ResetColor();

                menu.menu_glowne();

            }
            else
            {
                Console.ResetColor();
                Console.WriteLine("Gratulacje! Grasz dalej! :) ");
                Console.ReadKey(true);
                Console.Clear();
                Console.ResetColor();

                if (przechwycony_wynik > tablica_wynikow.odczytaj_wartosc(3, level))

                {
                    Console.WriteLine("Pobiłeś rekord! Twoje osiągnięcie {0} pkt na poziomie trudności: {1} ,\nzostanie zapisaane na Tablicy Rekordów!", przechwycony_wynik, menu.poziom(level));
                    tablica_wynikow.wpisz_wynik(3, level, Program.player, przechwycony_wynik);
                }
                else Console.WriteLine("Nie udało Ci się pobić rekordu. Obecny rekord możesz sprawdzić w menu.\n");
                
                Console.ReadKey(true);
            }
        }
    }
}